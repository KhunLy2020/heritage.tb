﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heritage
{
    class Program
    {
        static void Main(string[] args)
        {
            //Animal a1 = new Animal();
            Animal a2 = new Chat();
            Animal a3 = new Chien();
            //a1.Crier();
            a2.Crier();
            a3.Crier();


            Console.WriteLine();

            Terrestre2 a4 = new Chat2();
            Animal2 a5 = new Chien2();
            a4.Crier();
            a5.Crier();

            Console.ReadKey();
        }
    }
}
