﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heritage
{
    abstract class Animal
    {
        public abstract string Cri { get; }
        public void Crier()
        {
            Console.WriteLine(Cri);
        }
    }
}
