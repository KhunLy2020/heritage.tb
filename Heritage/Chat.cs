﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heritage
{
    class Chat : Animal
    {
        public override string Cri
        {
            get
            {
                return "Miaou";
            }
        }
    }
}
